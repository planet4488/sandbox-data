


provider "google" {
  credentials = "${file("sandbox-andiaye-ed54c009ec08.json")}"
  project     = "${var.project_id}"
  region      = "${var.zone_name}"
}

# This creates the google instance
resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance"
  zone    = "${var.zone_name}"
  machine_type = "${var.machine_size}"

  boot_disk {
    initialize_params {
      image = "${var.image_name}"
    }
  }

   network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }


   metadata_startup_script = "sudo apt-get update; sudo apt-get install -y curl lsb gnupg wget; echo deb http://packages.cloud.google.com/apt gcsfuse-stretch main | tee /etc/apt/sources.list.d/gcsfuse.list; cat /etc/apt/sources.list.d/gcsfuse.list; curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -; apt-get update; apt-get install -y gcsfuse"

  
}

