variable "project_id" {
  type        = "string"
  description = "Name of the project to instanciate the instance at."
  default     = "sandbox-andiaye"
}

variable "region_name" {
  type        = "string"
  description = "Region that this terraform configuration will instanciate at."
  default     = "europe-west1"
}

variable "zone_name" {
  type        = "string"
  description = "Zone that this terraform configuration will instanciate at."
  default     = "europe-west1-b"
}

variable "machine_size" {
  type        = "string"
  description = "The size that this instance will be."
  default     = "n1-standard-1"
}

variable "image_name" {
  type        = "string"
  description = "Kind of VM this instance will become"
  default     = "debian-cloud/debian-9"
}

variable "script_path" {
  type        = "string"
  description = "Where is the path to the script locally on the machine?"
}

variable "private_key_path" {
  type        = "string"
  description = "The path to the private key used to connect to the instance"
}

variable "username" {
  type        = "string"
  description = "The name of the user that will be used to remote exec the script"
}