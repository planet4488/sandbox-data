#!/bin/bash

# FROM atmoz/sftp
# MAINTAINER Michael Ghen <mike@mikeghen.com>

# Install FUSE so we can mount GCS buckets
# Ref: https://github.com/GoogleCloudPlatform/gcsfuse/blob/master/docs/installing.md
sudo apt-get update
sudo apt-get install -y curl lsb gnupg wget
echo "deb http://packages.cloud.google.com/apt gcsfuse-stretch main" | tee /etc/apt/sources.list.d/gcsfuse.list
cat /etc/apt/sources.list.d/gcsfuse.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
sudo apt-get update
sudo apt-get install -y gcsfuse