

# contain variables that are not suitable for SCM since they often vary in values depending on the system Terraform will be run in
# terraform.tfvars, that will override any of the values that we passed in variables.tf
# Variables that are dependendent on the user are declared here

script_path = "<SOME_PATH>"

private_key_path = "<SOME_PATH>"

username = "<some_value>"