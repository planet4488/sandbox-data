
terraform {
  backend "remote" {
    hostname = "app.terraform.io"  
    organization = "devoteamgcloud"

    workspaces {
      name = "test01"
    }
  }
}



################################
provider "google" {
  credentials = "${file(var.credentials)}"
  project = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}

data "google_client_config" "current" {}

resource "google_compute_network" "sftp_net" {
  name                    = "${var.network_name}"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "sftp_subnet" {
  name                     = "${var.network_name}-sub"
  ip_cidr_range            = "${var.ip_cidr_subnet_range}"
  network                  = "${google_compute_network.sftp_net.self_link}"
  region                   = "${var.region}"
  private_ip_google_access = true
}

data "google_container_engine_versions" "default" {
  location = "${var.zone}"
}


###############
// service account
resource "google_service_account" "sftp-account" {
  account_id = "sftp-account"
  display_name = "SFTP Account"
}
resource "google_service_account_key" "sftp-account-key" {
  service_account_id = "${google_service_account.sftp-account.name}"
}

resource "google_service_account" "scanner-account" {
    account_id = "scanner-account"
    display_name = "Scanner Account"
}

resource "google_service_account_key" "scanner-account-key" {
    service_account_id = "${google_service_account.scanner-account.name}"
}

################
// gke-cluster
resource "google_container_cluster" "primary" {
  name     = "my-gke-cluster"
  location = "${var.region}"
  network = "${google_compute_network.sftp_net.self_link}"
  subnetwork = "${google_compute_subnetwork.sftp_subnet.self_link}"
  remove_default_node_pool = true
  initial_node_count = 1
}


resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = "${var.region}"
  cluster    = "${google_container_cluster.primary.name}"
  node_count = 2
  node_config {
    machine_type = "n1-standard-1"
    metadata = {
      disable-legacy-endpoints = "true"
    }
    tags = ["${var.node_network_tag}"]
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
###################
resource "google_storage_bucket" "sftp-test-file-store" {
  name     = "sftp-test-file-store-bucket"
  location = "EU"
}

resource "google_storage_bucket" "sftp-test-file-clean" {
  name     = "sftp-test-file-clean-bucket"
  location = "EU"
}

resource "google_storage_bucket" "sftp-test-file-qua" {
  name     = "sftp-test-file-qua-bucket"
  location = "EU"
}

resource "google_storage_bucket_iam_binding" "iam-binding-sftp-account-store-bucket" {
  bucket = "${google_storage_bucket.sftp-test-file-store.name}"
  role        = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.sftp-account.email}",
  ]
}


resource "google_storage_bucket_iam_binding" "iam-binding-scanner-account-store-bucket" {
    bucket = "${google_storage_bucket.sftp-test-file-store.name}"
    role        = "roles/storage.objectViewer"

    members = [
        "serviceAccount:${google_service_account.scanner-account.email}",
    ]
}

resource "google_storage_bucket_iam_binding" "iam-binding-scanner-account-clean-bucket" {
  bucket = "${google_storage_bucket.sftp-test-file-clean.name}"
  role        = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.scanner-account.email}",
  ]
}

resource "google_storage_bucket_iam_binding" "iam-binding-scanner-account-qua-bucket" {
  bucket = "${google_storage_bucket.sftp-test-file-qua.name}"
  role        = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.scanner-account.email}",
  ]
}


resource "google_compute_firewall" "sftp-firewall-rule" {
  name    = "sftp-firewall"
  network = "${google_compute_network.sftp_net.self_link}"

  target_tags = ["${var.node_network_tag}"]

  allow {
    protocol = "tcp"
    ports    = ["30022"]
  }
}

