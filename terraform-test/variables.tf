
#################
### VARIABLES ###
#################

variable "region" {
  default = "us-west1"
}

variable "zone" {
  default = "us-west1-b"
}

variable "network_name" {
  default = "tf-gke"
}

variable "project" {
  default = "sandbox-andiaye"
}

variable "ip_cidr_subnet_range" {
  default = "10.127.0.0/20"
}

variable "credentials" {
  default = "sandbox-andiaye-d1e039233c8e.json"
}

variable "node_network_tag" {
  default = "sftp"
}
