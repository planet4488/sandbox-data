
#################
### OUTPUTS ###
#################


output "sftp-account-key" {
    value = "${google_service_account_key.sftp-account-key.private_key}"
}

output "scanner-account-key" {
    value = "${google_service_account_key.scanner-account-key.private_key}"
}