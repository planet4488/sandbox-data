provider "google" {
  credentials = file("sandbox-andiaye-d1e039233c8e.json")

  project = "sandbox-andiaye"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}
